import { Component, Input } from '@angular/core';

@Component({
  selector: 'monorepo-deployment-poc-lit-button',
  templateUrl: './lit-button.component.html',
  styleUrls: ['./lit-button.component.scss']
})
export class LitButtonComponent {
  @Input() buttonText = 'A Super Lit Button';
}
