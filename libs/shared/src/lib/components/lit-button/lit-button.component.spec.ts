import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LitButtonComponent } from './lit-button.component';

describe('LitButtonComponent', () => {
  let component: LitButtonComponent;
  let fixture: ComponentFixture<LitButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LitButtonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LitButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
