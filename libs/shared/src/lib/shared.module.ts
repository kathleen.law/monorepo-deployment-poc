import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LitButtonComponent } from './components/lit-button/lit-button.component';

@NgModule({
  imports: [CommonModule],
  declarations: [
    LitButtonComponent
  ],
  exports: [
    LitButtonComponent
  ]
})
export class SharedModule {}
