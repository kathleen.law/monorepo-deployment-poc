import { Component } from '@angular/core';

@Component({
  selector: 'monorepo-deployment-poc-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
}
