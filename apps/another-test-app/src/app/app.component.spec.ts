import { ComponentFixtureAutoDetect, TestBed } from '@angular/core/testing';
import { SharedModule } from '@monorepo-deployment-poc/shared';
import { AppComponent } from './app.component';
describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AppComponent],
      imports: [SharedModule]
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should toggle showFire', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.debugElement.nativeElement.querySelector('button').click(); 
    fixture.detectChanges();
    expect(fixture.debugElement.nativeElement.querySelector('p').textContent).toEqual("It's Lit!!!!");

    fixture.debugElement.nativeElement.querySelector('button').click(); 
    fixture.detectChanges();
    expect(fixture.debugElement.nativeElement.querySelector('p')).toBeNull();

  });
});
