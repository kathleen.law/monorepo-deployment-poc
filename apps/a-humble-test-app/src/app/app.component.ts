import { Component } from '@angular/core';

@Component({
  selector: 'monorepo-deployment-poc-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {

  aLitEvent() {
    alert("This isn't as lit as the event you'd get in another test app, but it's still pretty cool.");
  }
}
